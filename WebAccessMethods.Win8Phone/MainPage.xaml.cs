﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace WebAccessMethods.Win8Phone
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        #region HttpWebRequestWithCallback
        private void btnHttpWebRequestWithCallback_Click(object sender, RoutedEventArgs e)
        {
            // the URL we want to get our information from
            string uri = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22nome%2C%20ak%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

            // Create http request
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

            // Set up the continuation for the http request
            request.BeginGetResponse(HttpRequestCallback, request);
        }

        private async void HttpRequestCallback(IAsyncResult result)
        {
            // Retrieve the request
            HttpWebRequest request = result.AsyncState as HttpWebRequest;
            if (request != null)
            {
                try
                {
                    // Get the response
                    using (WebResponse response = request.EndGetResponse(result))
                    {
                        // Parse the response
                        ParseJsonWeatherString(response.GetResponseStream());
                    }
                }
                catch (Exception ex)
                {
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        txtForecast.Text = ex.Message;
                    });
                }
            }
        }

        private async void ParseJsonWeatherString(Stream stream)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(RootObject));
            RootObject weatherRoot = (RootObject)serializer.ReadObject(stream);
            Forecast forecast = weatherRoot.query.results.channel.item.forecast[3];

            // Switch to UI thread to update the label control
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                txtForecast.Text = $"Forecast for {forecast.date}: {forecast.text}";
            });
        }
        #endregion
    }
}
