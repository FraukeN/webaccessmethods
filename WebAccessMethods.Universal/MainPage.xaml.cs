﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WebAccessMethods.Universal
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        #region HttpWebRequestWithCallback
        private void btnHttpWebRequestWithCallback_Click(object sender, RoutedEventArgs e)
        {
            // the URL we want to get our information from
            string uri = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22nome%2C%20ak%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

            // Create http request
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

            // Set up the continuation for the http request
            request.BeginGetResponse(HttpRequestCallback, request);
        }

        private async void HttpRequestCallback(IAsyncResult result)
        {
            // Retrieve the request
            HttpWebRequest request = result.AsyncState as HttpWebRequest;
            if (request != null)
            {
                try
                {
                    // Get the response
                    using (WebResponse response = request.EndGetResponse(result))
                    {
                        // Parse the response
                        ParseJsonWeatherString(response.GetResponseStream());
                    }
                }
                catch (Exception ex)
                {
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        txtForecast.Text = ex.Message;
                    });
                }
            }
        }

        private async void ParseJsonWeatherString(Stream stream)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(RootObject));
            RootObject weatherRoot = (RootObject)serializer.ReadObject(stream);
            Forecast forecast = weatherRoot.query.results.channel.item.forecast[3];

            // Switch to UI thread to update the label control
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                txtForecast.Text = $"Forecast for {forecast.date}: {forecast.text}";
            });
        }
        #endregion
    }
}
